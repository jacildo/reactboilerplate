var React = require('react');
var ReactDOM = require('react-dom');

var obj = {
	name: "Bob",
	location: "There"
};
var obj2 = {
	age: "2",
	...obj
};

ReactDOM.render(
	<h1>React Boilerplate</h1>,
	document.getElementById('app')
);
