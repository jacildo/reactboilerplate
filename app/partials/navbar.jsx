<nav role="navigation" class="navbar navbar-default">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="#" class="navbar-brand">Brand</a>
    </div>
    <!-- Collection of nav links and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Messages</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Login</a></li>
        </ul>
    </div>
</nav>

var Navbar = React.createClass({
    getDefaultProps: function() {
        return {
            name: 'Bobby'
        }
    },
    getInitialState: function () {
        return {
            name: this.props.name
        };
    },
    onButtonClick: function (e) {
        e.preventDefault();

        var name = this.refs.name.value;
        this.refs.name.value = "";
        this.setState({
            name: name
        });
    },
    render: function () {
        // var links = this.links;
        var name = this.state.name;
        var links = this.props.links;

        return (
            <div>
                <div>{links}</div>
                <h1>Hello {name}!</h1>

                <form onSubmit={this.onButtonClick}>
                    <input type="text" ref="name"></input>
                    <button>Submit</button>
                </form>
            </div>
        );
    }
});

var links = [
    "Home",
    "About",
    "Contact"
];
var navbar = document.getElementById("navbar");
ReactDOM.render(
    <Navbar links={links} />,
    navbar
);
